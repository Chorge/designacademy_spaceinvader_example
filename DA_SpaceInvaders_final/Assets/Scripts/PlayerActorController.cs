﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerActorController : CombatActorController {

	/// <summary>
	/// The movement speed of the player
	/// </summary>
	[SerializeField] float m_movementSpeed = 1.0f;

	/// <summary>
	/// The limt how far the player can move to the right
	/// </summary>
	[SerializeField] float m_verticalMoveLimit = 3.36f;

	/// <summary>
	/// delay between shooting bullets
	/// </summary>
	[SerializeField] float m_lastBulletDelay;

	/// <summary>
	/// timestamp when the last bullet was fired
	/// </summary>
	float m_timeSinceLastBulletFired;

	/// <summary>
	/// the currrent game over UI element
	/// </summary>
	Text m_gameOverUI;

	// Use this for initialization
	void Start () {

		m_gameOverUI = GameObject.Find ("GameOverText").GetComponent<Text>();
		m_gameOverUI.gameObject.SetActive (false);

	}

	override protected void ControlMovement()
	{
		///calculate the current movement vector
		float movement = Input.GetAxis("Horizontal") * m_movementSpeed * Time.deltaTime;

		///do not let the player move outside this border
		Vector3 myPosition = this.transform.position;
		myPosition.x = Mathf.Clamp (myPosition.x + movement, -m_verticalMoveLimit, m_verticalMoveLimit);
		transform.position = myPosition;

		///create a projectile actor when space was pressed
		if (Input.GetButton ("Jump") && m_timeSinceLastBulletFired < Time.time - m_lastBulletDelay) 
		{
			FireProjectile ();

			m_timeSinceLastBulletFired = Time.time;
		}
	}

	override protected void TriggerDestruction()
	{
		m_gameOverUI.gameObject.SetActive (true);

		base.TriggerDestruction ();
	}
}
